package dominio;

import java.util.ArrayList;

public class Provincia{
	private String nombre;
	private ArrayList<Municipio> municipios = new ArrayList<>();

	public String getNombre(){
		return nombre;
	}

	public void setNombre(String nombre){
		this.nombre = nombre;
	}

	public void addMunicipio(Municipio municipio){
		municipios.add(municipio);
	}

	public int calcularNumeroHabitantes(){
		int habitantes = 0;
		for (Municipio municipio : municipios){
			habitantes += municipio.calcularNumeroHabitantes();
		}
		return habitantes;
	}

	public String toString(){
		String datos = "Provincia, " + nombre + " con " + 
			calcularNumeroHabitantes() + " habitantes con los siguientes municipios.";
		for (Municipio municipio : municipios){
			datos += "\n" + municipio;
		}
		return datos;
	}
}
