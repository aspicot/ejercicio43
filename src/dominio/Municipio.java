package dominio;
import java.util.ArrayList;

public class Municipio{
	private String nombre;
	private ArrayList<Localidad> localidades = new ArrayList<>();

	public String getNombre(){
		return nombre;
	}

	public void setNombre(String nombre){
		this.nombre = nombre;
	}

	public void addLocalidad(Localidad localidad){
		localidades.add(localidad);
	}

	public int calcularNumeroHabitantes(){
		int numeroDeHabitantes = 0;
		for(int i = 0; i < localidades.size(); i++){
			numeroDeHabitantes += localidades.get(i).getNumeroDeHabitantes();
		}
		return numeroDeHabitantes;
	}

	public String toString(){
		String datos = "Municipio, " + nombre + " con " + 
			calcularNumeroHabitantes() + " habitantes con las siguientes localidades:";
		for (Localidad loc : localidades){
			datos += "\n" + loc;
		}
		return datos;
	}
}
