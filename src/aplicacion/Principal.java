package aplicacion;

import dominio.*;

public class Principal{
	public static void main(String[] args){
		String[] nombre_municipios = {"Alcorcón", "Alcobendas"};
		String[] nombre_localidades = {"Alcorcón centro", "Alcorcón estación", "Alcorcón McDonalds", "Alcobendas centro", "Alcobendas estación", "Alcobendas Burger King"};
		int[] habitantes_localidades = {1000, 2000, 1500, 1250, 1500, 2300};
		

		Provincia provincia = new Provincia();
		provincia.setNombre("Madrid");

		for (int i = 0; i < nombre_municipios.length; i++){
			Municipio mun = new Municipio();
			mun.setNombre(nombre_municipios[i]);

			for(int j = i * 3; j < (i+1)*3;j++){
				Localidad loc = new Localidad();
				loc.setNombre(nombre_localidades[j]);
				loc.setNumeroDeHabitantes(habitantes_localidades[j]);
				mun.addLocalidad(loc);
			}
			provincia.addMunicipio(mun);
		}
		System.out.println(provincia);
	}
}
